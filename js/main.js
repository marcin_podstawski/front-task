var app = angular.module('suppliers', ['localytics.directives', 'angular-click-outside']);
app.controller('suppliers-ctrl', function($scope, $http){
    $scope.modalVisible = false;
    $scope.getData = function (page){
        $http.get("http://test-api.kuria.tshdev.io/", {
            params:  {query: $scope.queryText, rating: $scope.queryRating, page: page}
        }).then(function(response){
            $scope.appData = response.data;
        });
    }
    $scope.reset = function (){
        $scope.queryText = "";
        $scope.queryRating = "";
        $scope.getData();
    };
    $scope.pagination = function (key){
        var keyNumber = Number(key);
        var paginationArray = $scope.appData.pagination;
        var paginationStart = 0;
        var paginationTotal = paginationArray.total;
        var paginationFrom = paginationArray.from;
        var paginationTo = paginationArray.to;
        if (keyNumber == paginationStart){
            if (paginationFrom == paginationStart){
                return keyNumber;
            }
            return '<';
        }
        if (keyNumber == paginationTotal - 1){
            if (paginationTo == paginationTotal){
                return keyNumber;
            }
            return '>';
        }
        return keyNumber;
    };
    $scope.showModal = function (number){
        var modal = angular.element( document.querySelector( '#modal-' + number ) );
        modal.addClass('visible');
        $scope.modalVisible = true;
    };
    $scope.hideModal = function (){
        var modals = angular.element(document.querySelectorAll('.modal .info'));
        modals.removeClass('visible');
        $scope.modalVisible = false;
    };
    $scope.getData();
});
app.filter('range', function(){
    return function(input, min, max){
        min = parseInt(min);
        max = parseInt(max);
        for (var i = min; i < max; i++)
            input.push(i);
        return input;
    };
});