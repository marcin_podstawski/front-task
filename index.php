<!DOCTYPE html>
<html>
<head>
    <title>Where your money goes</title>
    <script src="js/jquery/jquery-3.1.1.min.js"></script>
    <script src="chosen/chosen.jquery.min.js"></script>
    <link rel="stylesheet" href="chosen/chosen.min.css">
    <link rel="image_src" href="chosen/chosen-sprite.png">
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/grid12.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <script src="js/angular/angular.min.js"></script>
    <script src="js/angular/angular-chosen.js"></script>
    <script type="text/javascript" src="js/angular/clickoutside.directive.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body ng-app="suppliers" ng-controller="suppliers-ctrl">
    <div class="page-wrapper">
        <div class="page">
            <h1 class="text-center area">Where your money goes?</h1>
            <h3 class="text-center">Payments made by Chichester District Council to individual suppliers with a value over &pound;500 made within October.</h3>
            <hr class="">
            <ul class="menu text-center area">
                <input type="text" ng-model="queryText" ng-change="getData()" placeholder="Search suppliers" class="search-input item">
                <li class="select item">
                    <select chosen disable-search="true" ng-model="queryRating" ng-change="getData()" ng-options="item for item in [] | range:1:6" data-height="('30px')">
                        <option value="">Select pound rating</option>
                    </select>
                </li>
                <li class="reset button item" ng-click="reset()">Reset</li>
                <li class="search button item" ng-click="getData()">Search</li>
            </ul>
            <div class="area area-table">
                <div class="area-table-row head">
                    <div class="area-table-cell">
                        <span class="visible-xs">Suppliers</span>
                        <span class="hidden-xs">Supplier</span>
                    </div>
                    <div class="area-table-cell hidden-xs">
                        Pound rating
                    </div>
                    <div class="area-table-cell hidden-xs">
                        Reference
                    </div>
                    <div class="area-table-cell hidden-xs">
                        Value
                    </div>
                </div>
                <div class="area-table-row data-row" ng-repeat="(key, data) in appData.payments" ng-click="showModal(key)">
                    <div class="area-table-cell">
                        <div class="visible-xs title">Supplier:</div>
                        {{ data.payment_supplier }}
                    </div>
                    <div class="area-table-cell">
                        <div class="visible-xs title">Rating:</div>
                        <?php //div zamknięty w nastepnej linii ze względu na znak biały ?>
                        <div class="rating-container">
                            <div ng-repeat="n in [] | range:0:data.payment_cost_rating" class="rating highlighted text-center">&pound;
                            </div><div ng-repeat="n in [] | range:0:5 - data.payment_cost_rating" class="rating text-center">&pound;</div>
                        </div>
                    </div>
                    <div class="area-table-cell">
                        <div class="visible-xs title">Reference:</div>
                        {{ data.payment_ref }}
                    </div>
                    <div class="area-table-cell">
                        <div class="visible-xs title">Value:</div>
                        &pound;{{ data.payment_amount }}
                    </div>
                </div>
            </div>
            <ul class="area pagination text-center" ng-if="appData.pagination.from != appData.pagination.to - 1">
                <li ng-repeat="(key, data) in appData.pagination.links" ng-click="getData(key)" ng-class="key == appData.pagination.current ? 'current' : ''">
                    <span> {{ pagination(key) }} </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="modal" ng-class="{'modalVisible' : modalVisible}">
        <div class="content" click-outside="hideModal()" outside-if-not="data-row">
            <div class="header">
                Details
                <div class="close">
                    <div class="close-ico" ng-click="hideModal()">
                    </div>
                </div>
            </div>
            <div id="modal-{{ key }}" class="info" ng-repeat="(key, data) in appData.payments">
                <h5>{{ data.payment_supplier }}</h5>
                <div class="area-table-cell">
                    <div class="title">Rating:</div>
                    <?php //div zamknięty w nastepnej linii ze względu na znak biały ?>
                    <div class="rating-container">
                        <div ng-repeat="n in [] | range:0:data.payment_cost_rating" class="rating highlighted text-center">&pound;
                        </div><div ng-repeat="n in [] | range:0:5 - data.payment_cost_rating" class="rating text-center">&pound;</div>
                    </div>
                </div>
                <div class="area-table-cell">
                    <div class="title">Reference:</div>
                    {{ data.payment_ref }}
                </div>
                <div class="area-table-cell">
                    <div class="title">Value:</div>
                    &pound;{{ data.payment_amount }}
                </div>
            </div>
        </div>
    </div>
</body>
</html>